$('.to-top_arrow').on('click', function () {
    $("html, body").animate({ scrollTop: 0 }, "slow");
});

$('.main-block_slider').slick({
    fade: true,
    cssEase: 'linear',
    autoplay: true,
    autoplaySpeed: 5000,
    prevArrow: '<button type="button" class="main-block_slider__prev"></button>',
    nextArrow: '<button type="button" class="main-block_slider__next"></button>'
});

$('.reviews-carousel').slick({
    slidesToShow: 3,
    slidesToScroll: 3,
    dots: true,
    prevArrow: '<button type="button" class="general_arrow arrow_prev"><span class="arrow"></span></button>',
    nextArrow: '<button type="button" class="general_arrow arrow_next"><span class="arrow"></span></button>',
    responsive: [
        {
            breakpoint: 6000,
            settings: {
                slidesToShow: 3,
                slidesToScroll: 3
            }
        },
        {
            breakpoint: 1024,
            settings: {
                variableWidth: false,
                slidesToShow: 1,
                slidesToScroll: 1
            }
        },
        {
            breakpoint: 768,
            settings: {
                variableWidth: false,
                slidesToShow: 1,
                slidesToScroll: 1,
                arrows: false
            }
        }
    ]
});

$(window).on('load resize', function() {
    if (window.outerWidth < 768) {
        $('.cards-slider').not('.slick-initialized').slick({
            arrows: false,
            dots: true,
            slidesToShow: 1
        });
        $('.partners_carousel').not('.slick-initialized').slick({
            arrows: false,
            dots: true,
            slidesToShow: 1,
            rows: 3
        });

    } else {

        $('.slick-initialized').not('.reviews-carousel').not('.main-block_slider').slick('unslick');
    }
});

$(document).ready(function () {
    function openMobileMenu() {
        $('.header_menu__mobile').slideDown();
        $('.line.before , .line.after').hide();
        $('.line').addClass('arrow');
        $('.header_button').addClass('close');
    }
    function closeMobileMenu() {
        $('.header_button').removeClass('close');
        $('.line').removeClass('arrow');
        setTimeout(function () {
            $('.line.before , .line.after').show();
        }, 500);
        $('.header_menu__mobile').slideUp();
    }
    $('.header_button').on('click', function () {
        if ($(this).hasClass('close')) {
            closeMobileMenu();
        } else {
            openMobileMenu();
        }
    });

    $('.show-more-teachers').on('click', function () {
        $('.side-bar-content-teachers-item').show();
        $(this).hide();
    });
});
// Bodymovin animation
$(document).ready(function() {
    var chessClub = document.getElementById('9737');
    var chessBlock = document.getElementById('block-9737');
    var chessClubParams = {
        container: chessClub,
        path: 'wp-content/themes/itrocks/img/data2.json',
        renderer: 'svg',
        loop: false,
        autoplay: false,
        name: "Chess club",
    };

    var corporateEng = document.getElementById('9736');
    var corporateEngBlock = document.getElementById('block-9736');
    var corporateEngParams = {
        container: corporateEng,
        path: 'wp-content/themes/itrocks/img/data3.json',
        renderer: 'svg',
        loop: false,
        autoplay: false,
        name: "Corporate English",
    };

    var itCourses = document.getElementById('9734');
    var itCoursesBlock = document.getElementById('block-9734');
    var itCoursesParams = {
        container: itCourses,
        path: 'wp-content/themes/itrocks/img/data1.json',
        renderer: 'svg',
        loop: false,
        autoplay: false,
        name: "IT Courses",
    };

    var kidsEng = document.getElementById('9735');
    var kidsEngBlock = document.getElementById('block-9735');
    var kidsEngParams = {
        container: kidsEng,
        path: 'wp-content/themes/itrocks/img/data4.json',
        renderer: 'svg',
        loop: false,
        autoplay: false,
        name: "Kids English",
    };

    var chessAnimation = bodymovin.loadAnimation(chessClubParams);
    chessBlock.addEventListener("mouseenter", chessStart);
    chessBlock.addEventListener("mouseleave", chessFinish);

    function chessStart(){
        chessAnimation.setDirection(1);
        chessAnimation.play();
    }

    function chessFinish(){
        chessAnimation.setDirection(-1);
        chessAnimation.play();
    };


    var corporateEngAnimation = bodymovin.loadAnimation(corporateEngParams);
    corporateEngBlock.addEventListener("mouseenter", corporateStart);
    corporateEngBlock.addEventListener("mouseleave", corporateFinish);

    function corporateStart(){
        corporateEngAnimation.setDirection(1);
        corporateEngAnimation.play();
    }

    function corporateFinish(){
        corporateEngAnimation.setDirection(-1);
        corporateEngAnimation.play();
    };

    var itCoursesAnimation = bodymovin.loadAnimation(itCoursesParams);
    itCoursesBlock.addEventListener("mouseenter", itCoursesStart);
    itCoursesBlock.addEventListener("mouseleave", itCoursesFinish);

    function itCoursesStart(){
        itCoursesAnimation.setDirection(1);
        itCoursesAnimation.play();
    }

    function itCoursesFinish(){
        itCoursesAnimation.setDirection(-1);
        itCoursesAnimation.play();
    };

    var kidsEngAnimation = bodymovin.loadAnimation(kidsEngParams);
    kidsEngBlock.addEventListener("mouseenter", kidsEngStart);
    kidsEngBlock.addEventListener("mouseleave", kidsEngFinish);

    function kidsEngStart(){
        kidsEngAnimation.setDirection(1);
        kidsEngAnimation.play();
    }

    function kidsEngFinish(){
        kidsEngAnimation.setDirection(-1);
        kidsEngAnimation.play();
    };
});