'use strict';

const gulp = require('gulp');

const plumber = require('gulp-plumber');
const less = require('gulp-less');
const rename = require('gulp-rename');
const cleanCSS = require('gulp-clean-css');
const newer = require("gulp-newer");
const imagemin = require("gulp-imagemin");
const jsuglify = require('gulp-uglify');
const rigger = require('gulp-rigger');
const watcher = require('gulp-watch');
const del = require('del');

var path = {
    build: {
        html: 'design/build/',
        js: 'design/build/js/',
        css: 'design/build/css/',
        img: 'design/build/img/',
        fonts: 'design/build/fonts/'
    },
    src: {
        html: 'design/src/*.html',
        js: 'design/src/js/main.js',
        style: 'design/src/css/main.less',
        img: 'design/src/img/**/*.*',
        fonts: 'design/src/fonts/**/*.*'
    },
    watch: {
        html: 'design/src/**/*.html',
        js: 'design/src/js/**/*.js',
        style: 'design/src/css/**/*.less',
        img: 'design/src/img/**/*.*',
        fonts: 'design/src/fonts/**/*.*'
    },
    production: {
        js: 'src/www/js/',
        css: 'src/www/css/',
        img: 'src/www/img/',
        fonts: 'src/www/fonts/'
    },
    deploy: {
        style: 'src/www/css/',
        js: 'src/www/js'
    },
    clean: './build'
};

// Clean
function clean() {
    return del([path.clean]);
}

// CSS
function css() {
    return gulp
        .src(path.src.style)
        .pipe(plumber())
        .pipe(less())
        .pipe(cleanCSS({compatibility: 'ie8'}))
        .pipe(rename({ suffix: ".min" }))
        .pipe(gulp.dest(path.build.css))
        .pipe(gulp.dest(path.production.css));
}

// Оптимизация изображений
function images() {
    return gulp
        .src(path.src.img)
        .pipe(
            imagemin([
                imagemin.gifsicle({ interlaced: true }),
                imagemin.jpegtran({ progressive: true }),
                imagemin.optipng({ optimizationLevel: 5 }),
                imagemin.svgo({
                    plugins: [
                        {
                            removeViewBox: false,
                            collapseGroups: true
                        }
                    ]
                })
            ])
        )
        .pipe(gulp.dest(path.build.img))
        .pipe(gulp.dest(path.production.img));
}

// JS
function js() {
    return gulp
        .src(path.src.js)
        .pipe(plumber())
        .pipe(rigger())
        .pipe(jsuglify())
        .pipe(rename({suffix: ".min"}))
        .pipe(gulp.dest(path.build.js))
        .pipe(gulp.dest(path.production.js));
}

// HTML
function html() {
    return gulp
        .src(path.src.html)
        .pipe(rigger())
        .pipe(gulp.dest(path.build.html));
}

// Watcher
function watchFiles() {
    gulp.watch(path.watch.style, css);
    gulp.watch(path.watch.js, js);
    // gulp.watch(path.watch.img, images);
    gulp.watch(path.watch.html, html);
    gulp.watch([
            path.watch.style,
            path.watch.js,
            // path.watch.img,
            path.watch.html
        ],
        //browserSyncReload
    );
}

// Задаем паралельный таски
const build = gulp.series([clean, gulp.parallel(js, css, images, html)]);
const defaultTask = gulp.parallel([watchFiles, build]);
const watch = gulp.parallel(watchFiles);


// Экспортируем таски
exports.css = css;
exports.clean = clean;
exports.js = js;
exports.html = html;
exports.images = images;
exports.build = build;
exports.watch = watch;
exports.default = defaultTask;